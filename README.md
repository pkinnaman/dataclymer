# dataclymer

SQL Questions
1. How many customers (users) are there?
2. Test the prior result. Are there any duplicate entries? Can you think of multiple ways to check
for duplicates?
3. Provide a report that shows order status and a count of how many orders have each status.
4. Provide an ordered report of the top 10 customers as determined by dollar amount sold.
Include the first name, last name, user_id, and total dollars spent. HINT:: we only want
uncancelled and unreturned values counted. BONUS: format the results nicely (i.e. with a
dollar sign and two decimal places)
5. What is the brand and item_name of the most expensive product for each category type? Are
there any ties? Does anything concern you about the results?


